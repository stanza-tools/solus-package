pkgdir=$1
sharedir=$2
etcdir=$3

cat << EOF > $etcdir/.stanza
install-dir = "$sharedir"
platform = linux
pkg-dirs = ("$pkgdir/pkgs")
fast-pkg-dirs = ("$pkgdir/fast-pkgs")
EOF
