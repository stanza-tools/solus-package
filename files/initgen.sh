pkgdir=$1
etcdir=$2

cat << EOF > $etcdir/profile.d/stanza.sh
export STANZA_CONFIG=$etcdir
export STANZA_FILE=$STANZA_CONFIG/.stanza
export STANZA_PKGS=$pkgdir/pkgs
export STANZA_FAST_PKGS=$pkgdir/fast-pkgs
EOF

